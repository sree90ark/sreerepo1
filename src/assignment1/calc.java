package assignment1;
import java.util.Scanner;
public class calc
{
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		
		/*Write Java program to allow 
		 * the user to input two integer values and
		 *  then the program prints the results of adding,
		 *   subtracting, multiplying, and dividing among the two values.
		 */
		
		int a,b;
		String name;
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter your name");
		name=sc.nextLine();
		System.out.println("Enter a number");
		a=sc.nextInt();
		System.out.println("Enter another number");
		b=sc.nextInt();
		
		sc.close();
		System.out.println("Hello "+name);
		System.out.println("Addition : " +(a+b));
		System.out.println("Subtraction :" +(a-b));
		System.out.println("Multiplication :" +(a*b));
		System.out.println("Division:" +(a/b));
		System.out.println("Modulus :" +(a%b));
	}

}
