package sree_package;

public class booleanTest
{
	public static void main(String[] args) 
	{
		//Use the boolean keyword to declare boolean variable
		boolean testResult;
		testResult=true;
		//Initialize the boolean variable with value true or false, once boolean declaration is done
		

		//Print the value of boolean variable
		System.out.println("Test Result is: " +testResult);

		//Change the value of boolean variable
		  testResult=false;

		//Print the value of boolean variable
		System.out.println("Test Result is: " +testResult);
		}
 }
