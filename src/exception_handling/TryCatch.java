package exception_handling;

public class TryCatch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int a=40;
		int b=0;
		
		try
		{
			int c=a/b;
			System.out.println(c);
		}
		catch(ArithmeticException e)
		{
			System.out.println(e); System.out.println("hello");
			System.exit(0);

		}
				finally
		{System.out.println("finally block is always excecuted whether the exception was handled or not");}
	
		System.out.println("Even though there is an exception, rest of the code is executed");
	}
	

}
