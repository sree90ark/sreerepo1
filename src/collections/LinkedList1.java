package collections;

import java.util.Iterator;
import java.util.LinkedList;

public class LinkedList1 {

	public static void main(String[] args) {
		
		LinkedList<String> L = new LinkedList<>();
		L.add("Hello");
		L.add("Welcome");
		L.add("Home");
		L.addFirst("good morning");
		L.addLast("good night");
		L.add(2,"and");
		
		// TODO Auto-generated method stub
		Iterator<String> I = L.iterator();
		while(I.hasNext())
		{
			System.out.println(I.next());
		}

		System.out.println("\nAfter removal\n");
		L.remove("Hello");
		L.removeFirst();
		L.removeLast();
		L.remove(1);
		
		
		Iterator<String> It = L.iterator();
		while(It.hasNext())
		{
			System.out.println(It.next());
		}
		
	}

}
