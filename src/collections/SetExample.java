package collections;

import java.util.*;


public class SetExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//HashSet<String> s = new HashSet<String>();
		//LinkedHashSet<String> s = new LinkedHashSet<String>();
		TreeSet<String> s = new TreeSet<String>();
		s.add("hello");
		s.add("welcome");
		s.add("to");
		s.add("my");
		s.add("home");
		
		System.out.println(s);
		
		Iterator<String> i = s.iterator();
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
	}

}